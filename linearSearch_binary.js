function liniarSearchBinary(arr, val) {

    var start = 0
    var end = arr.length - 1;
    var middle = Math.floor((start + end) / 2)

    //console.log(start, middle, end)

    while (arr[middle] !== val && start <= end) {
        if (val < arr[middle]) {
            end = middle - 1
        } else {
            start = middle + 1
        }
        middle = Math.floor((start + end) / 2)
        console.log(start, middle, end)
    }

    if (arr[middle] === val) {
        console.log(middle)
    } else { console.log(-1) }
}

liniarSearchBinary([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 32, 33, 34, 35], 34)
liniarSearchBinary([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20], 11)
liniarSearchBinary([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20], 9)
liniarSearchBinary([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20], 3)