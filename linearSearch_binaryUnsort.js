function liniarSearchBinaryUnsort(arr, val) {

    let sorting = arr.sort((a, b) => {return a - b});
    //console.log(sorting);

    var start = 0
    var end = sorting.length - 1;
    var middle = Math.floor((start + end) / 2)

    //console.log(start, middle, end)

    while (arr[middle] !== val && start <= end) {
        if (val < arr[middle]) {
            end = middle - 1
        } else {
            start = middle + 1
        }
        middle = Math.floor((start + end) / 2)
        console.log(start, middle, end)
    }

    if (arr[middle] === val) {
        console.log(middle)
    } else { console.log(-1) }
}

liniarSearchBinaryUnsort([24, 36, 45, 12, 25, 1, 3, 8, 5, 4, 9, 18, 16, 45, 55, 36, 11, 14, 21, 66], 9)
liniarSearchBinaryUnsort([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20], 3)