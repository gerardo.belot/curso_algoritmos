
function areThereDuplicates() {
    let collection = {}
    for (let val in arguments) {
        console.log("val " + val + " " + arguments[val])
        collection[arguments[val]] = (collection[arguments[val]] ||  0) + 1
        console.log("after " + collection[arguments[val]])
    }

    for (let key in collection) {
        if (collection[key] > 1) return true
    }
    return false;
}


console.log(areThereDuplicates(1, 2, 3, 4)) // false
console.log(areThereDuplicates(1, 2, 2, 2)) // true 
console.log(areThereDuplicates('a', 'b', 'c', 'a')) // true 