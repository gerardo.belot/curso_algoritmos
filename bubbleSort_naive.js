"use strict";
const swap = (arr, idx1, idx2) => {
    [arr[idx1], arr[idx2]] = [arr[idx2], arr[idx1]]
}

const bubbleSort = (arr) => {
    console.log(arr);
    let noSwap;
    for (let i = arr.length; i > 0; i--) {
        noSwap = true;
        for (let j = 0; j < i - 1; j++) {
            //console.log(arr[j], arr[j+1], noSwap)
            if(arr[j] > arr[j+1]){
                swap(arr, j, j + 1)
                noSwap = false
            }
        }
        if(noSwap) break
    }

    return arr
}

console.log(bubbleSort([37, 45, 29, 8, 25, 60, 12, 10, -3]))
//console.log(bubbleSort([6, 1, 2, 3, 4, 5]))