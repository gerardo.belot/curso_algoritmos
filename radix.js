const getDigit = (num, i) => {
    return Math.floor(Math.abs(num) / Math.pow(10, i)) % 10;
}

const digitCount = (num) => {
    if (num === 0) return 1
    return Math.floor(Math.log10(Math.abs(num))) + 1
}

const mostDigit = (nums) => {
    let maxDIgit = 0
    for (let i = 0; i < nums.length; i++) {
        maxDIgit = Math.max(maxDIgit, digitCount(nums[i]));
    }

    return maxDIgit;
}

const radixSort = (nums) => {
    let maxDigitCount = mostDigit(nums);
    for (let k = 0; k < maxDigitCount; k++) {
        let digitBuckets = Array.from({ length: 10 }, () => [])
        for (let i = 0; i < nums.length; i++) {
            let digits = getDigit(nums[i], k)
            digitBuckets[digits].push(nums[i])
        }
        nums = [].concat(...digitBuckets)
    }
    return nums
}

console.log(radixSort([25, 345, 5467, 12, 2345, 9852, 64587]))