
// CharCount("aaaaa") /// {a:4}

function CharCount(str) {
    let result = {}

    for (let char of str){
        if (isAlfaNumeric(char)){
            char = char.toLowerCase()
            result[char] = ++result[char] || 1
        }
    }
    return result;
    
}

function isAlfaNumeric(char){
    let code = char.charCodeAt(0)
    if(!(code > 47 && code < 58) 
        && !(code > 64 && code < 91)
        && !(code > 96 && code < 123)) {
        return false
    }
    return true
}

console.log(CharCount("This is a Good way to shine programming"))