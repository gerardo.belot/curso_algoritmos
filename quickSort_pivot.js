const pivot = (arr, start = 0, end = arr.length + 1) => {
    function swap(array, i, j) {
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp
    }

    var pivot = arr[start]
    var swapIdx = start;

    for (let i = start + 1; i < arr.length; i++) {
        if (pivot > arr[i]) {
            swapIdx++
            swap(arr, swapIdx, i)
        }
    }
    swap(arr, start, swapIdx)
    return swapIdx
}

const quickSort = (arr, left = 0, right = arr.length - 1) => {
    let pivotIndex = pivot(arr, left, right)

    if (left < right) {
        // left
        quickSort(arr, left, pivotIndex - 1)
        // rigth
        quickSort(arr, pivotIndex + 1, right)
    }

    return arr

}

console.log(quickSort([4, 8, 2, 1, 5, 7, 6, 3]))