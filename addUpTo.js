function addUpTo(n) {
    let total = 0;
    for(let i = 1; i <= n; i++) {
        total += i
    }

    return total
}

function addUpTo2(n){
    return n * (n + 1) / 2;
}

let t1 = performance.now()
console.log(addUpTo2(10))
let t2 = performance.now()



//addUpTo2(500)

console.log(`Time elapsed: ${(t2 - t1) / 1000} secunds`)