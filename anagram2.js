

function anagram(str1, str2){

    if(str1.length != str2.length){
        return false
    }

    const lookup = {}

    for(let i = 0; i < str1.length; i++){
        let letter = str1[i];

        if(lookup[letter]){
            lookup[letter] += 1
        } else {
            lookup[letter] = 1
        }
    }

    for(let i = 0; i < str2.length; i++){
        let char = str2[i];
        if(!lookup[char]){
            return false
        } else {
            lookup[char] -= 1
        }
    }

    return true
}

console.log(anagram("manda", "namda")) // true
console.log(anagram("manda", "namdas")) //false

