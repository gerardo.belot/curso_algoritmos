
/**
 * Given two strings, write a function to determine if the
 * secund string is an anagram of the first. An anagram is a word, 
 * pharse or name firmed by rearrangin the letters of another,
 * such as cinema formed fro iceman
 * 
 * validAnagram("", "") // true
 * validAnagram('aaz', 'zza') // false
 * validAnagram('rat', 'cat') // false
 * validAnagram('rat', 'tar') // true
 * validAnagram('zoom', 'mooz') // true
 */

function validAnagram(str1, str2) {

    if (str1.length !== str2.length) {
        return false;
    }

    // creamos un array para guardar las letras
    let lookup = {}; 
    /**
     * Este bloque corre primero
     * construye el objeto que contendra el conteo
     */
    for (var i = 0; i < str1.length; i++) {
        let letter = str1[i];
        // si la letra existe, incrementa, si no marca 1
        lookup[letter] ? lookup[letter] += 1 : lookup[letter] = 1;
    }

    // loop al string 2
    for (var j = 0; j < str2.length; j++) {
        let letter = str2[j];

        // if no existe lookup false
        if (!lookup[letter]) {
            return false
        } else {
            // si lo encuentra lo elimina 
            lookup[letter] -= 1;
        }
    }
    // devolvemos true si elimina todas las letras en lookup
    return true
}

console.log(validAnagram("", ""))
console.log(validAnagram('aaz', 'zza'))
console.log(validAnagram('rat', 'tar'))
console.log(validAnagram('zoom', 'mozo'))
console.log(validAnagram('anagrams', 'nagarams'))
console.log(validAnagram('anagrams', 'nagaramm'))



