function sameFrequency(num1, num2) {

    let arr1 = num1.toString();
    let arr2 = num2.toString();

    if (arr1.length != arr2.length) return false;

    let loockUp = {};

    for (let i = 0; i < arr1.length; i++) {
        let letter = arr1[i];
        loockUp[letter] ? loockUp[letter] += 1 : loockUp[letter] = 1;
    }

    for (let j = 0; j < arr2.length; j++) {
        let letter = arr2[j];

        if (!loockUp[letter]) {
            return false;
        } else {
            loockUp[letter] -= 1;
        }
    }

    return true;

}

console.log(sameFrequency(182, 281)) // true
console.log(sameFrequency(34, 14)) // false
console.log(sameFrequency(3589578, 5879385)) // true
console.log(sameFrequency(22, 222)) // false